const initState = {
    movieAdd: ''
};

const rateReducer = (state = initState, action) => {
    switch(action.type){
        case 'RATE_ADDING_SUCCESS':
            console.log('add success!');
            return {
                ...state,
                movieAdd: 'added' ,
            };
        case 'RATE_ADDING_ERROR':
            console.log('error');
            return{
                ...state,
                movieAdd: false
            };
        case 'RATE_UPDATE_SUCCESS':
            console.log('updated success!');
            return{
                ...state,
                movieAdd: 'updated',
            };
        case 'RATE_UPDATE_ERROR':
            console.log('update error!');
            return{
                ...state,
                movieAdd: false
            };
        case 'GET_ID':
            console.log(action.seenMovie);
            return{
                ...state,
                idRating: action.idRating,
                seen: action.seenMovie,
                watched: action.watchedMovie
            };
        case 'SEEN_UPDATE_SUCCESS':
            console.log(action.seenMovie);
            return{
                ...state,
                seen:action.seenMovie,
            };
        case 'WATCHED_UPDATE_SUCCESS':
            console.log(action.watchedMovie);
            return{
                ...state,
                watched:action.watchedMovie,
            }
        default:
            return state;
    }
};

export default rateReducer;