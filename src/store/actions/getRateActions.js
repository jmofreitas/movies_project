export const getRateAction = (rate) => {
    return (dispatch, getState, {getFirestore}) => {
        const firestore = getFirestore();
        // const firebase = getFirebase();
        // const profile = getState().firebase.profile;
        const authorId = getState().firebase.auth.uid;
        // firestore.collection('user-ratings').doc(
        firestore.collection('user-ratings').where('idUser', '==', authorId).get().then(snapshot => {
            if (snapshot.empty){
              console.log('empty');
                firestore.collection('user-ratings').add({
                    idMovie: rate.idmovie,
                    idUser: authorId,
                    rating: rate.rating,
                    ratedAt: new Date()
                }).then(() => {
                    dispatch({type: 'RATE_ADDING_SUCCESS', movieId :rate.idmovie});
                }).catch(err => {
                    dispatch({type: 'RATE_ADDING_ERROR'}, err);
                });
            }
            else{
            console.log('not_empty')
            }
        })
    }
};