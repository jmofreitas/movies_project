export const rateMovie = (rate) => {
    return (dispatch, getState, {getFirestore}) => {
        const firestore = getFirestore();
        let docId;
        // const firebase = getFirebase();
        // const profile = getState().firebase.profile;
        const authorId = getState().firebase.auth.uid;
        // firestore.collection('user-ratings').doc(
        if(authorId !== undefined) {
            if (rate.todo === 'like') {
                firestore.collection('user-ratings').where('idUser', '==', authorId).where('idMovie', '==', rate.idmovie).get().then(snapshot => {
                    if (snapshot.empty) {
                        console.log('No Match, saving');
                        if (rate.rating !== 101) {
                            firestore.collection('user-ratings').add({
                                idMovie: rate.idmovie,
                                idUser: authorId,
                                rating: rate.rating,
                                ratedAt: new Date()
                            }).then(() => {
                                dispatch({type: 'RATE_ADDING_SUCCESS', movieId: rate.idmovie});
                            }).catch(err => {
                                dispatch({type: 'RATE_ADDING_ERROR'}, err);
                            });
                        }
                    } else {

                        snapshot.forEach(function (doc) {
                            docId = doc.id;
                            console.log(docId);
                        });
                        if (rate.type === 'update') {
                            firestore.collection('user-ratings').doc(docId.toString()).update({
                                idMovie: rate.idmovie,
                                idUser: authorId,
                                rating: rate.rating,
                                ratedAt: new Date()
                            }).then(() => {
                                dispatch({
                                    type: 'RATE_UPDATE_SUCCESS', movieId: rate.idmovie
                                })
                            }).catch(err => {
                                dispatch({
                                        type: 'RATE_UPDATE_ERROR'
                                    }, err
                                )
                            })
                        } else if (rate.type === 'get') {
                            firestore.collection('user-ratings').doc(docId.toString()).get()
                                .then(doc => {
                                    if (!doc.exists) {
                                        console.log('No such document!');
                                    } else {

                                        dispatch({
                                            type: 'GET_ID',
                                            idRating: doc.data().rating,
                                            seenMovie: doc.data().seen,
                                            watchedMovie: doc.data().watched
                                        })
                                    }
                                })
                                .catch(err => {
                                    console.log('Error getting document', err);
                                });
                        }
                    }
                });
            } else if (rate.todo === 'seen') {
                firestore.collection('user-ratings').where('idUser', '==', authorId).where('idMovie', '==', rate.idmovie).get().then(snapshot => {
                    if (snapshot.empty) {
                        firestore.collection('user-ratings').add({
                            idMovie: rate.idmovie,
                            idUser: authorId,
                            seen: rate.seenValue,
                            ratedAt: new Date(),
                        }).then(() => {
                            dispatch({type: 'RATE_ADDING_SUCCESS', movieId: rate.idmovie});
                        }).catch(err => {
                            dispatch({type: 'RATE_ADDING_ERROR'}, err);
                        });
                    } else {

                        snapshot.forEach(function (doc) {
                            docId = doc.id;
                            console.log(docId);
                        });
                        if (rate.type === 'update') {
                            firestore.collection('user-ratings').doc(docId.toString()).update({
                                idMovie: rate.idmovie,
                                idUser: authorId,
                                seen: rate.seenValue,
                                ratedAt: new Date()
                            }).then(() => {
                                dispatch({
                                    type: 'SEEN_UPDATE_SUCCESS', movieId: rate.idmovie, seenMovie: rate.seenValue
                                })
                            }).catch(err => {
                                dispatch({
                                        type: 'RATE_UPDATE_ERROR'
                                    }, err
                                )
                            })
                        } else if (rate.type === 'get') {
                            firestore.collection('user-ratings').doc(docId.toString()).get()
                                .then(doc => {
                                    if (!doc.exists) {
                                        console.log('No such document!');
                                    } else {
                                        dispatch({
                                            type: 'GET_ID', idRating: doc.data().rating
                                        })
                                    }
                                })
                                .catch(err => {
                                    console.log('Error getting document', err);
                                });
                        }
                    }
                });
            } else if (rate.todo === 'watched') {
                firestore.collection('user-ratings').where('idUser', '==', authorId).where('idMovie', '==', rate.idmovie).get().then(snapshot => {
                    if (snapshot.empty) {
                        firestore.collection('user-ratings').add({
                            idMovie: rate.idmovie,
                            idUser: authorId,
                            watched: rate.watchedValue,
                            ratedAt: new Date(),
                        }).then(() => {
                            dispatch({type: 'RATE_ADDING_SUCCESS', movieId: rate.idmovie});
                        }).catch(err => {
                            dispatch({type: 'RATE_ADDING_ERROR'}, err);
                        });
                    } else {

                        snapshot.forEach(function (doc) {
                            docId = doc.id;
                        });
                        if (rate.type === 'update') {
                            firestore.collection('user-ratings').doc(docId.toString()).update({
                                idMovie: rate.idmovie,
                                idUser: authorId,
                                watched: rate.watchedValue,
                                ratedAt: new Date()
                            }).then(() => {
                                dispatch({
                                    type: 'WATCHED_UPDATE_SUCCESS',
                                    movieId: rate.idmovie,
                                    watchedMovie: rate.watchedValue
                                })
                            }).catch(err => {
                                dispatch({
                                        type: 'RATE_UPDATE_ERROR'
                                    }, err
                                )
                            })
                        } else if (rate.type === 'get') {
                            firestore.collection('user-ratings').doc(docId.toString()).get()
                                .then(doc => {
                                    if (!doc.exists) {
                                    } else {
                                        dispatch({
                                            type: 'GET_ID', idRating: doc.data().rating
                                        })
                                    }
                                })
                                .catch(err => {
                                    console.log('Error getting document', err);
                                });
                        }
                    }
                });
            }
        }
    }
};