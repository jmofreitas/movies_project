import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import {Link} from "react-router-dom";
import { connect } from 'react-redux'
import { signOut } from '../../store/actions/authActions'



const styles = {
    root: {
        flexGrow: 1,
    },
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
        primary: '#171717',
};

class NavBar extends React.Component {
    state = {
        auth: true,
        anchorEl: null,
    };

    handleMenu = event => {
        this.setState({anchorEl: event.currentTarget});
    };

    handleClose = () => {
        this.setState({anchorEl: null});
    };

    render() {
        const {classes, auth} = this.props;
        const {anchorEl} = this.state;
        const open = Boolean(anchorEl);
        console.log(auth);
        let logged;
        if (auth.isEmpty !== true) {
            logged = <div>
                <IconButton
                    aria-owns={open ? 'menu-appbar' : undefined}
                    aria-haspopup="true"
                    onClick={this.handleMenu}
                    color="inherit"
                >
                    <AccountCircle/>
                </IconButton>
                <Menu
                    style={{marginTop:50}}
                    id="menu-appbar"
                    anchorEl={anchorEl}
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                    }}
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                    }}
                    open={open}
                    onClose={this.handleClose}
                >
                    <MenuItem onClick={this.handleClose}>Profile</MenuItem>
                    <MenuItem onClick={this.handleClose}>My account</MenuItem>
                    <Link to="/">
                    <MenuItem onClick={this.props.signOut}>Sign Out</MenuItem>
                    </Link>
                </Menu>
            </div>;
        }
        else {
            logged = <div>
                <Link disableUnderline={true} to='/signin' className='link-normal'>
                <IconButton
                    aria-owns={open ? 'menu-appbar' : undefined}
                    aria-haspopup="true"
                    onClick={this.handleMenu}
                    color="inherit"
                >
                    <Typography variant="h6" color="inherit" className={classes.grow}>
                        <a style={{textDecoration: 'none', color: 'white'}} href="">Login</a>
                    </Typography> </IconButton>
                </Link>

            </div>;
        }

        return (
            <div className={classes.root}>
                <AppBar position="static" style={{backgroundColor: '#212121'}}>
                    <Toolbar>
                        <Typography variant="h6" color="inherit" className={classes.grow}>
                            <Link disableUnderline={true} to='/' className='link-normal'>

                            Movie Search App
                        </Link>
                        </Typography>

                        {logged}
                    </Toolbar>
                </AppBar>
            </div>
        );
    }
}

NavBar.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
    // console.log(state);
    return{
        auth: state.firebase.auth,
        profile: state.firebase.profile
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        signOut: () => dispatch(signOut())
    }
};


export default connect(mapStateToProps,  mapDispatchToProps)(withStyles(styles)(NavBar));