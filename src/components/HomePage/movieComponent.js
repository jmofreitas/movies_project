import React, {Component} from 'react';
import { Link } from "react-router-dom";
import Grid from "@material-ui/core/Grid/Grid";
import '../../App.css'
import Typography from "@material-ui/core/es/Typography/Typography";
import Button from "@material-ui/core/es/Button/Button";



class MovieComponent extends Component {
    render() {
        let title;
        let name;
        let toLink;
        let overview;
        if(this.props.movie.type2 !== undefined) {
            toLink = '/' + this.props.movie.type2.toLowerCase() + '/' + this.props.movie.id;
        }
        else{
            toLink = '/' + this.props.movie.media_type + '/' + this.props.movie.id;

        }
        let release_date = this.props.movie.release_date;
        let first_air = this.props.movie.first_air_date;

        if(this.props.movie.media_type === undefined){
            if(this.props.movie.type2 !== undefined){
                if(this.props.movie.type2 === 'Tv'){
                    name = 'Tv Show';
                    title = this.props.movie.name;
                }
                else{
                    name = 'Movie';
                    title = this.props.movie.title;

                }

            }
        }
        else {
            if (this.props.movie.media_type === "tv") {
                title = this.props.movie.original_name;
                name = "TV Show";

            } else {
                title = this.props.movie.title;
                name = "Movie";

            }
        }
            if (this.props.movie.overview != undefined) {
                overview = (this.props.movie.overview).substring(0, 150) + '...';
            } else {
                overview = "No description for this " + name;
            }
        return (

            <Grid item xs={6} sm={4} md={3} lg={3} alignItems='center' justify='center'
                  style={{textAlign: 'center', padding: 10}}>
                <Grid style={{textAlign: 'center'}}  className='grid-main'>
                    <Grid className='img__wrap'>
                        <img style={{width: '100%'}} alt={"poster" + this.props.movie.id}
                             src={this.props.movie.Poster}/>
                        <Grid className='img__description_layer'>
                            <Grid className='img__description' style={{width: '90%', height: '90%'}}>
                                {/*<Grid xs={12}>*/}
                                {/*<Typography variant='body1' style={{*/}
                                {/*borderBottomColor: 'white',*/}
                                {/*borderBottomWidth: 2,*/}
                                {/*borderBottomStyle: 'solid',*/}
                                {/*paddingBottom: 5,*/}
                                {/*marginBottom: 0,*/}
                                {/*color: 'white',*/}
                                {/*fontSize: '1.1sem'*/}
                                {/*}}>{title}</Typography>*/}
                                {/*</Grid>*/}
                                <Grid container spacing={0} style={{minHeight: '100%'}}>
                                    <Grid container spacing={24} className='mobile-hide'>
                                        {/*<Grid item xs={4}>*/}
                                            {/*{movieTV}*/}
                                        {/*</Grid>*/}
                                        <Grid item xs={6}>
                                            {/*<p className='margin-less'>{name}</p>*/}
                                            <Typography variant="body1" className='margin-less c-white' align='center'
                                                        justify='center'>
                                                {name}
                                            </Typography>
                                        </Grid>
                                        <Grid item xs={6}>
                                            {/*<p className='margin-less'>8.7 <i className='fas fa-star'></i></p>*/}
                                            <Typography variant='body1' className='margin-less c-white'>
                                                8.7 <i className='fas fa-star'></i>
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                    <Grid item xs={12} style={{marginTop: 10}}>
                                        {/*<h5 className='text-ident margin-less'>{overview}</h5>*/}
                                        <Typography className='text-ident c-white' variant='body1'>
                                            {overview}
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={12} className='margin-less' style={{minHeight:'100%'}}>
                                        <Link disableUnderline={true} to={toLink}>

                                        <Button variant='contained' className='btn-position'>
                                            See More
                                        </Button>
                                        </Link>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography variant='subtitle2' align='left' style={{color:'white'}}>
                            {release_date}{first_air}
                        </Typography>
                        <Typography variant='subtitle1' align='left' style={{color:'white'}}>
                            {title}
                        </Typography>
                </Grid>
            </Grid>
        {/*<div>*/
        }
        {/*<Modal*/
        }
        {/*style={{translate: '30%', translateZ: '600', rotate: '10deg'}}*/
        }
        {/*aria-labelledby="simple-modal-title"*/
        }
        {/*aria-describedby="simple-modal-description"*/
        }
        {/*open={this.state.open}*/
        }
        {/*>*/
        }
        {/*<Grid container*/
        }
        {/*spacing={0}*/
        }
        {/*direction="column"*/
        }
        {/*alignItems="center"*/
        }
        {/*justify="center"*/
        }
        {/*style={{minHeight: '100vh'}}>*/
        }

        {/*<Grid container spacing={0} xs={12} sm={9} style={{backgroundColor: 'white'}}>*/
        }
        {/*<div style={{width: '100%', height: '100%', padding: 20}}  ref={node => this.node = node}>*/
        }

        {/*<Grid item xs={4} className='margin-less'>*/
        }
        {/*{movieTV}*/
        }
        {/*</Grid>*/
        }
        {/*<Grid item xs={4} className='margin-less'>*/
        }
        {/*/!*<p className='margin-less'>{name}</p>*!/*/
        }
        {/*<Typography variant='h5' className='margin-less'>*/
        }
        {/*{name}*/
        }
        {/*</Typography>*/
        }
        {/*</Grid>*/
        }
        {/*<Grid item xs={4} className='margin-less'>*/
        }
        {/*/!*<p className='margin-less'>8.7 <i className='fas fa-star'></i></p>*!/*/
        }
        {/*<Typography variant="h5" className='margin-less'>*/
        }
        {/*8.7 <i className='fas fa-start'></i>*/
        }
        {/*</Typography>*/
        }
        {/*</Grid>*/
        }
        {/*<Grid item xs={12} className='margin-less'>*/
        }
        {/*/!*<h5 className='text-ident'>{overview}</h5>*!/*/
        }
        {/*<Typography className='text-ident margin-less' variant='body1' gutterBottom>*/
        }
        {/*{overview}*/
        }
        {/*</Typography>*/
        }
        {/*</Grid>*/
        }
        {/*</div>*/
        }
        {/*</Grid>*/
        }
        {/*</Grid>*/
        }
        {/*</Modal>*/
        }
        {/*</div>*/
        }
    </Grid>

    )
        ;
    }
}


export default MovieComponent;