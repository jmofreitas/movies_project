import React, {Component} from 'react';
import $ from 'jquery';
import Pagination from 'rc-pagination';
import MovieComponent from "./movieComponent";
import Grid from "@material-ui/core/Grid/Grid";
import 'rc-pagination/assets/index.css';
import 'rc-select/assets/index.css';



class SearchMain extends Component {
    constructor(props) {
        super(props);
        this.state = {
            stateSearch: '',
            page: '',
            current: 1
        }
    }

    onChange = (page) => {
       this.setState({current: page});
    };

    paginationTo1(){
        this.setState({current: 1});
    }

    componentDidMount(){
        this.doUpdate();

    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.searchType === 1) {
            if (this.props.search !== prevProps.search) {
                if (this.props.search.length > 2) {
                    this.paginationTo1();
                    this.doUpdate();
                }
            }
        }
        else if(this.props.searchType === 2) {
            if (this.props.searchFields !== prevProps.searchFields) {
                this.paginationTo1();
                this.doUpdate();
            }
            if(this.props.mediaType !== prevProps.mediaType){
                this.paginationTo1();
                this.doUpdate();
            }
        }
        if (prevState.current !== this.state.current) {
            this.doUpdate();
        }
    }


    doUpdate() {
        // const urlString = "https://api.themoviedb.org/3/search/multi?api_key=f634db3b23ebcd55d865db0de4f007e7&query=" + searchTerm;
        const searchString = this.props.search;
        let pageString;
        let urlString;
        let genre = '';
        let year = '';
        let sortby = '';
        if (this.props.searchType === 1) {
            if (this.state.current === 1) {
                pageString = '&page=1';
            } else {
                pageString = '&page=' + this.state.current;
            }
            urlString = "https://api.themoviedb.org/3/search/multi?api_key=f634db3b23ebcd55d865db0de4f007e7&query=" + searchString + pageString;
        } else {
            if (this.state.current === 1) {
                pageString = '&page=1';
            } else {
                pageString = '&page=' + this.state.current;
            }
            //&with_genres=16,28&sort_by=popularity.desc&year=2015";
            if (this.props.searchFields.genre !== '') {
                genre = '&with_genres=';
                for (let i = 0, len = this.props.searchFields.genre.length; i < len; i++) {
                    genre += this.props.searchFields.genre[i] + ',';
                }
            } else {
                genre = '';
            }
            if (this.props.searchFields.year !== '') {
                year = '&primary_release_year=' + this.props.searchFields.year;
            } else {
                year = '';
            }
            if (this.props.searchFields.sortby !== '') {
                sortby = '&sort_by=' + this.props.searchFields.sortby;
            } else {
                sortby = '';
            }
            urlString = "https://api.themoviedb.org/3/discover/"+this.props.mediaType.toLowerCase()+"?api_key=f634db3b23ebcd55d865db0de4f007e7" + genre + year + sortby + pageString;
        }
            $.ajax({
                url: urlString,
                success: (searchResults) => {
                    if (searchResults.Response !== "False") {
                        const results = searchResults.results;
                        const pagination = searchResults.total_results;
                        let previousID;
                        let movieDivs = [];
                        results.forEach((movie) => {
                            if (previousID !== movie.id) {
                                if (movie.poster_path === null || movie.poster_path === undefined) {
                                    movie.Poster = "img/poster_photo.jpg"
                                } else {
                                    movie.Poster = 'https://image.tmdb.org/t/p/w185_and_h278_bestv2/' + movie.poster_path;
                                }
                                if(this.props.searchType === 2){
                                    movie.type2 = this.props.mediaType;
                                }
                                const movieDiv = <MovieComponent key={movie.id} movie={movie}/>;
                                movieDivs.push(movieDiv)
                            }
                            // this.setState({previousId: movie.imdbID});
                            previousID = movie.id;
                        });
                        this.setState({rows: movieDivs});
                        this.pagination(pagination);
                    }
                },
                error: (xhr, status, err) => {
                    console.log("fetch falhou");
                }
            });
    }

    pagination(totalResults) {
        if (this.props.search !== undefined) {
            if (this.props.search.length > 2) {
                const pagination =
                    <Pagination onChange={this.onChange}
                        // selectComponentClass={Select}
                                current={this.state.current}
                                showSizeChanger
                                defaultPageSize={20}
                                defaultCurrent={1}
                                total={parseInt(totalResults)}/>;
                this.setState({pagination: pagination});
            } else {
                const pagination = '';
                this.setState({pagination: pagination});
            }
        }
            else if(this.props.searchFields !== undefined){
            const pagination =
                <Pagination onChange={this.onChange}
                    // selectComponentClass={Select}
                            current={this.state.current}
                            showSizeChanger
                            defaultPageSize={20}
                            defaultCurrent={1}
                            total={parseInt(totalResults)}/>;
            this.setState({pagination: pagination});
        } else {
            const pagination = '';
            this.setState({pagination: pagination});
        }
    }
    render() {
        return (
            <div>
                <Grid  container
                       spacing={0}
                       direction="column"
                       alignItems="center"
                       justify="center"
                       style={{ minHeight: '100vh',

                       }}>

                <Grid container spacing={0} xs={12} sm={10} lg={8} style={{backgroundColor: 'rgba(78,62,145, 0.7)'
                    ,marginTop: 20, marginBottom: 20}}>
                    {this.state.rows}
                    {/*<Grid item xs={6} xsPull={6}>*/}
                    {/*<Row className="show-grid" style={{display: 'inline-block'}}>*/}
                    {/*{this.state.rows}*/}
                    {/*{this.state.pagination}*/}
                    {/*</Row>*/}
                    {/*</Grid>*/}
                </Grid>

                {this.state.pagination}
                </Grid>

            </div>


        );
    }
}

export default SearchMain;