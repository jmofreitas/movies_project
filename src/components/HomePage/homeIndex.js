import React, {Component} from 'react';
import NavBar from "../NavBar/NavBar";
import Grid from '@material-ui/core/Grid/Grid'
import SearchBar from "../../customStyling/searchBar";
import SearchMain from "./SearchMain";
import SearchYear from "../SearchComponents/searchYear";
import SearchGenre from "../SearchComponents/searchGenre";
import SearchSortBy from "../SearchComponents/searchSortBy";
import Button from "@material-ui/core/es/Button/Button";



let typeTimer;
let typeInterval = 500;

class HomeIndex extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchType: 1,
            offset: 0,
            search: '',
            select: '',
            mediaType:'Movie',


            searchWithFields :{
                year: '',
                genre: '',
                sortby: '',
            }
        };
    }
    onChange(field, value) {
        this.setState({ searchWithFields: {
                ...this.state.searchWithFields,
                year: '',
                genre: '',
                sortby:'',
            },});
        this.setState({searchType: 1});
        // parent class change handler is always called with field name and value
        clearTimeout(typeTimer);
        typeTimer = setTimeout(() => this.searchIt(field, value), typeInterval);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(prevState.search !== this.state.search) {
            if (this.state.searchType === 2) {
                this.popSelect(2);
            }
            else{
                this.popSelect(1);
            }
        }
    }

    buttonClick(type){
        this.setState({mediaType: type});
        setTimeout(() => {
            const propSearch = <SearchMain searchFields={this.state.searchWithFields} searchType={this.state.searchType}  mediaType={this.state.mediaType}/>;
            this.setState({search: propSearch});
            }, 10);

    }

    popSelect(number){
        if(number === 2) {
            this.setState({
                select: <Grid  container
                               spacing={0}
                               direction="column"
                               alignItems="center"
                               justify="center"
                               style={{ minWidth: '100vw',
                                   marginBottom:20
                               }}>>
                    <Grid item xs ={6} style={{marginBottom: -20, backgroundColor:'white', padding:0}}>
                        <Button onClick={() => this.buttonClick('Movie')} variant='contained' name='movie' className='purple-btn'>
                            Movie
                        </Button>
                        <Button onClick={() => this.buttonClick('Tv')} variant='contained' name='tv' className='purple-btn'>
                            TV
                        </Button>
                    </Grid>
                </Grid>
            });
        }
        else if (number === 1){
            this.setState({select: ''});
        }
    }
    onChangeType(type, value) {
                this.setState({searchType: 2});
                if(type === 'year'){
                    this.setState({ searchWithFields: {
                            ...this.state.searchWithFields,
                            year: value,
                        },});
                    const propSearch = <SearchMain searchFields={this.state.searchWithFields} searchType={this.state.searchType} mediaType={this.state.mediaType}/>;
                    this.setState({search: propSearch});
                }
                else if(type === 'genre'){
                    this.setState({ searchWithFields: {
                            ...this.state.searchWithFields,
                            genre: value,
                        },});
                    const propSearch = <SearchMain searchFields={this.state.searchWithFields} searchType={this.state.searchType}  mediaType={this.state.mediaType}/>;
                    this.setState({search: propSearch});
                }
                else if(type === 'sortby'){
                    this.setState({ searchWithFields: {
                            ...this.state.searchWithFields,
                            sortby: value,
                        },});
                    const propSearch = <SearchMain searchFields={this.state.searchWithFields} searchType={this.state.searchType}  mediaType={this.state.mediaType}/>;
                    this.setState({search: propSearch});
                }

        // const propWithFields = <SearchMain searchType={searchWithFields}/>;
        // this.setState({search: propWithFields});

    }

    searchIt(getField, getValue) {
        const propSearch = <SearchMain search={getValue} searchType={this.state.searchType}/>;
        this.setState({search: propSearch});
    }

    render() {
        return (
            <div style={{backgroundColor: '#fff'}}>
                <NavBar/>

                <Grid container
                      spacing={0}
                      direction="column"
                      alignItems="center"
                      justify="center"
                      style={{backgroundColor: '#4e3e91', minHeight: 300}}>

                    <Grid item xs={12}>
                        <img src='img/popsearch.png' style={{width: 120}}/>
                    </Grid>
                    <Grid item xs={12}>
                        <SearchBar
                            onChange={this.onChange.bind(this)}/>
                    </Grid>
                    <Grid item xs={12}>
                        <Grid container
                              spacing={0}
                              alignItems="center"
                              justify="center">
                            <Grid item xs={4} sm={4}>
                                <SearchYear onChange={this.onChangeType.bind(this)}/>
                            </Grid>
                            <Grid item xs={4} sm={4}>
                                <SearchGenre onChange={this.onChangeType.bind(this)}/>
                            </Grid>
                            <Grid item xs={4} sm={4}>
                                <SearchSortBy onChange={this.onChangeType.bind(this)}/>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                {this.state.mediaType}
                {this.state.select}
                {this.state.search}
            </div>
        )
            ;
    }
}


export default HomeIndex;