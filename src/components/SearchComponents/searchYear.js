import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const styles = theme => ({
    button: {
        display: 'block',
        marginTop: theme.spacing.unit * 2,
        borderBottomColor: 'white'
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
        color: 'white'
    },

});

class SearchYear extends React.Component {
    state = {
        year: '',
        open: false,
    };


    handleChange = event => {
        if (event.target.value !== this.state.year) {
            this.setState({[event.target.name]: event.target.value});
        }
        setTimeout(() => {
            this.props.onChange(event.target.name, event.target.value);
        }, 10);
    };

    handleClose = () => {
        this.setState({open: false});
    };

    handleOpen = () => {
        this.setState({open: true});
    };
    readValues = () => {
        let table = [];

        for (let i = 2019; i > 1900; i--) {
            table.push(<MenuItem value={i}>{i}</MenuItem>);
        }
        return table;
    };


    render() {
        const {classes} = this.props;
        return (
            <form autoComplete="off">
                <FormControl className={classes.formControl}>
                    <InputLabel style={{color: 'white'}} htmlFor="demo-controlled-open-select">Year</InputLabel>
                    <Select
                        open={this.state.open}
                        onClose={this.handleClose}
                        onOpen={this.handleOpen}
                        value={this.state.year}
                        onChange={this.handleChange}
                        inputProps={{
                            name: 'year',
                            id: 'demo-controlled-open-select',
                        }}
                        style={{color: 'white'}}
                    >
                        <MenuItem value="">
                            <em>None</em>
                        </MenuItem>
                        {this.readValues()}
                    </Select>
                </FormControl>
            </form>
        );
    }
}

SearchYear.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SearchYear);