import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const styles = theme => ({
    button: {
        display: 'block',
        marginTop: theme.spacing.unit * 2,
        borderBottomColor: 'white'
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
        color: 'white'
    },


});

class SearchSortBy extends React.Component {
    state = {
        sortby: '',
        open: false,
    };


    handleChange = event => {
        if (event.target.value !== this.state.year) {
            this.setState({[event.target.name]: event.target.value});
        }
        setTimeout(() => {
            this.props.onChange(event.target.name, event.target.value);
        }, 10);
    };

    handleClose = () => {
        this.setState({open: false});
    };

    handleOpen = () => {
        this.setState({open: true});
    };



    render() {
        const {classes} = this.props;
        return (
            <form autoComplete="off">
                <FormControl className={classes.formControl}>
                    <InputLabel style={{color: 'white'}} htmlFor="demo-controlled-open-select">Sort By</InputLabel>
                    <Select
                        open={this.state.open}
                        onClose={this.handleClose}
                        onOpen={this.handleOpen}
                        value={this.state.sortby}
                        onChange={this.handleChange}
                        inputProps={{
                            name: 'sortby',
                            id: 'demo-controlled-open-select',
                        }}
                        style={{color: 'white'}}
                    >
                        <MenuItem value="">
                            <em>None</em>
                        </MenuItem>
                        <MenuItem value='popularity.asc'>
                            Popularity Asc
                        </MenuItem>
                        <MenuItem value='popularity.desc'>
                            Popularity Desc
                        </MenuItem>
                        <MenuItem value='release_date.asc'>
                            Release Date Asc
                        </MenuItem>
                        <MenuItem value='release_date.desc'>
                            Release Date Desc
                        </MenuItem>
                        <MenuItem value='revenue.asc'>
                            Revenue Asc
                        </MenuItem>
                        <MenuItem value='revenue.desc'>
                            Revenue Desc
                        </MenuItem>
                        <MenuItem value='primary_release_date.asc'>
                            Primary Release Date Asc
                        </MenuItem>
                        <MenuItem value='primary_release_date.desc'>
                            Primary Release Date Desc
                        </MenuItem>
                        <MenuItem value='original_title.asc'>
                            Original Title Asc
                        </MenuItem>
                        <MenuItem value='original_title.desc'>
                            Original Title Desc
                        </MenuItem>
                        <MenuItem value='vote_average.asc'>
                            Vote Average Asc
                        </MenuItem>
                        <MenuItem value='vote_average.desc'>
                            Vote Average Desc
                        </MenuItem>
                        <MenuItem value='vote_count.asc'>
                            Vote Count Asc
                        </MenuItem>
                        <MenuItem value='vote_count.desc'>
                            Vote Count Desc
                        </MenuItem>
                    </Select>
                </FormControl>
            </form>
        );
    }
}

SearchSortBy.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SearchSortBy);