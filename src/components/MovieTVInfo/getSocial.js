import React, {Component} from 'react';
import Grid from "@material-ui/core/Grid/Grid";
import $ from "jquery";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

class GetSocial extends Component {
    state={
        writeSocial:{
            imdb: '',
            facebook:'',
            instagram: '',
            twitter: ''
        }
    };
    componentDidMount(){
        let pathId = this.props.id;
        let urlString2;
        console.log(this.props);
        if(this.props.type === 'movie'){
            urlString2 = "https://api.themoviedb.org/3/movie/"+pathId+"/external_ids?api_key=f634db3b23ebcd55d865db0de4f007e7"
        }
        else if(this.props.type === 'tv'){
            urlString2 = "https://api.themoviedb.org/3/tv/"+pathId+"/external_ids?api_key=f634db3b23ebcd55d865db0de4f007e7"

        }
        $.ajax({
            url: urlString2,
            success: (searchResults) => {
                this.setState({socialMedia: searchResults});
                console.log(searchResults);
                this.checkSocial();

            },
            error: (xhr, status, err) => {
                console.log(xhr, status, err);
                console.log("fetch falhou");
            }
        });
    }



    checkSocial(){
        let imdb;
        let facebook;
        let instagram;
        let twitter;
        if(this.state.socialMedia.imdb_id !== null){
            let href = "https://www.imdb.com/title/" + this.state.socialMedia.imdb_id;
            imdb = <Grid item xs={2} align='right'>
                <a href={href} rel='noopener noreferrer' target='_blank' style={{color: 'white', textDecoration: 'none'}}>
                    <FontAwesomeIcon icon={['fab', 'imdb']} style={{fontSize: '2em'}}/>
                </a>
            </Grid>
        }
        else{
            imdb = <Grid item xs={2} align='right'>
                    <FontAwesomeIcon icon={['fab', 'imdb']} style={{fontSize: '2em', color:'grey'}}/>
            </Grid>
        }
        if(this.state.socialMedia.facebook_id !== null){
            let href = 'https://www.facebook.com/' + this.state.socialMedia.instagram_id;
            facebook =  <Grid item xs={2} align='right'>
                <a href={href} rel='noopener noreferrer' target='_blank' style={{color: 'white', textDecoration: 'none'}}>
                    <FontAwesomeIcon icon={['fab', 'facebook-square']} style={{fontSize: '2em'}}/>
                </a>
            </Grid>
        }
        else{
            facebook =  <Grid item xs={2} align='right'>
                <FontAwesomeIcon icon={['fab', 'facebook-square']} style={{fontSize: '2em', color:'grey'}}/>
            </Grid>
        }
        if(this.state.socialMedia.instagram_id !== null){
            let href = 'https://www.instagram.com/' + this.state.socialMedia.instagram_id;
            instagram =  <Grid item xs={2} align='right'>
                <a href={href} rel='noopener noreferrer' target='_blank' style={{color: 'white', textDecoration: 'none'}}>
                    <FontAwesomeIcon icon={['fab', 'instagram']} style={{fontSize: '2em'}}/>
                </a>
            </Grid>
        }
        else{
            instagram =  <Grid item xs={2} align='right'>
                    <FontAwesomeIcon icon={['fab', 'instagram']} style={{fontSize: '2em', color:'grey'}}/>

            </Grid>
        }
        if(this.state.socialMedia.twitter_id !== null){
            let href= 'https:/www.twitter.com/' + this.state.socialMedia.twitter_id;
            twitter =  <Grid item xs={2} align='right'>
                <a href={href} rel='noopener noreferrer' target='_blank' style={{color: 'white', textDecoration: 'none'}}>
                    <FontAwesomeIcon icon={['fab', 'twitter']} style={{fontSize: '2em'}}/>
                </a>
            </Grid>
        }
        else{
            twitter =  <Grid item xs={2} align='right'>
                    <FontAwesomeIcon icon={['fab', 'twitter']} style={{fontSize: '2em', color:'grey'}}/>
            </Grid>
        }
        this.setState({ writeSocial: {
                ...this.state.writeSocial,
                imdb: imdb,
                facebook: facebook,
                instagram: instagram,
                twitter: twitter
            },});
    }

    render() {
        return (
            <Grid container alignItems='center' justify='center'>
                {this.state.writeSocial.imdb}
                {this.state.writeSocial.facebook}
                {this.state.writeSocial.instagram}
                {this.state.writeSocial.twitter}
            </Grid>
        );
    }
}

export default GetSocial;