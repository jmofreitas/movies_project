import { Progress } from 'react-sweet-progress';
import "react-sweet-progress/lib/style.css";

import React, {Component} from 'react';

class TimeProgress extends Component {
    render() {
        return (
            <Progress
                theme={{
                    active:{
                        symbol: <span style={{color:'yellow'}}>◴</span>,
                        trailColor: 'rgb(255, 255, 255)',
                        color:'yellow'
                    }
                }}
                percent={this.props.percent}
                />
        );
    }
}

export default TimeProgress;