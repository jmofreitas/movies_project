import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import MuiExpansionPanel from '@material-ui/core/ExpansionPanel';
import MuiExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import MuiExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Grid from "@material-ui/core/Grid/Grid";
import $ from "jquery";

const ExpansionPanel = withStyles({
    root: {
        boxShadow: 'none',
        '&:not(:last-child)': {
            borderBottom: 0,
        },
        '&:before': {
            display: 'none',
        },


    },
    expanded: {
        margin: 'auto',
    },
})(MuiExpansionPanel);

const ExpansionPanelSummary = withStyles({
    root: {
        backgroundColor: 'rgba(25, 25, 25, 0.9)',
        borderBottom: '3px solid rgba(0,0,0,.125)',
        marginBottom: -1,
        minWidth: '100vw',
        minHeight: 56,
        '&$expanded': {
            minHeight: 56,
        },
    },
    content: {
        '&$expanded': {
            margin: '12px 0',
        },
    },
    expanded: {},
})(props => <MuiExpansionPanelSummary {...props} />);

ExpansionPanelSummary.muiName = 'ExpansionPanelSummary';

const ExpansionPanelDetails = withStyles(theme => ({
    root: {
        padding: theme.spacing.unit * 2,
    },
}))(MuiExpansionPanelDetails);

class MovieExpansionAcc extends React.Component {
    state = {
        expanded: 'panel1',
        panel: true,
        cast: [],
        searchDone: false,
        writeSeasons: 'Indisponível nos Movies :) Ups, não ficou terminado!'
    };

    handleChange = panel => (event, expanded) => {
        this.setState({
            expanded: expanded ? panel : false,
        });
    };


    getSeason(){
        if(this.state.writeSeasons === 'Indisponível nos Movies :) Ups, não ficou terminado!') {
            if(this.props.type === 'tv') {
                if (this.props.info !== undefined) {
                    if (this.props.info.length > 0) {
                        let allStuff = [];
                        for (let i = 0, len = this.props.info.length; i < len; i++) {
                            let stuff = <Grid item xs={10} style={{border: '2px solid black'}}>
                                <Grid container alignItems='center' justify='center'>
                                    <Grid item xs={6}>
                                        <img
                                            src={'https://image.tmdb.org/t/p/original/' + this.props.info[i].poster_path}
                                            style={{width: '50%'}}/>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Grid container alignItems='center' justify='center'>
                                            <Grid item xs={12} style={{marginBottom: 25}}>
                                                {this.props.info[i].name} with {this.props.info[i].episode_count} episodes
                                            </Grid>
                                            <Grid item xs={12}>
                                                {this.props.info[i].overview}
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>;
                            allStuff.push(stuff);
                        }
                        this.setState({writeSeasons: allStuff})

                    }
                }
            }
        }
    }
    componentWillUpdate(nextProps, nextState, nextContext) {
        this.getSeason();
    }

    componentDidMount(){
        this.getSeason();
        setTimeout(() => {
                this.getCast();
        }, 1000);

    }

    getCast(){
        let urlString;
        let pathId = this.props.id;
        if(this.props.type === 'movie'){
            urlString = 'https://api.themoviedb.org/3/movie/'+pathId+'/casts?api_key=f634db3b23ebcd55d865db0de4f007e7'
        }
        else if (this.props.type === 'tv'){
            urlString = 'https://api.themoviedb.org/3/tv/'+pathId+'/credits?api_key=f634db3b23ebcd55d865db0de4f007e7'

        }
        $.ajax({
            url: urlString,
            success: (searchResults) => {
                let allCasts=[];
                if (searchResults.Response !== "False") {
                    let searchCast = searchResults.cast;
                    searchCast.forEach((cast) => {
                        allCasts.push(cast);
                    })
                }
                this.setState({cast: allCasts});

            },
            error: (xhr, status, err) => {
                console.log("fetch falhou");
            }
            });
    }

    render() {
        const { expanded } = this.state;
        return (
            <div>
                <ExpansionPanel expanded={expanded === 'panel1'} onChange={this.handleChange('panel1')}>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon style={{color:'white'}} />}>
                        <Grid container alignItems='center' justify='center'>
                            <Grid item xs={4}>
                                <Typography align='left' style={{color:'white'}}>Seasons</Typography>
                            </Grid>
                            <Grid item xs={4}>
                            </Grid>
                            <Grid item xs={4}>
                                <Typography align='right' style={{color:'white'}}>View Seasons</Typography>
                            </Grid>
                        </Grid>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Typography>
                            <Grid container alignItems='center' justify='center'>
                                {this.state.writeSeasons}
                            </Grid>
                        </Typography>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel expanded={expanded === 'panel2'} onChange={this.handleChange('panel2')}>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon style={{color:'white'}} />}>
                        <Grid container alignItems='center' justify='center'>
                        <Grid item xs={4}>
                        <Typography align='left' style={{color:'white'}}>Crew</Typography>
                        </Grid>
                            <Grid item xs={4}>
                            </Grid>
                            <Grid item xs={4}>
                        <Typography align='right' style={{color:'white'}}>Teste</Typography>
                        </Grid>
                        </Grid>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Typography>
                            <Grid container alignItems='center' justify='center'>
                            {/*{this.props.info.overview}*/}
                            {this.state.cast.map((cast) => {
                                if(cast.profile_path !== null) {
                                    return (
                                        <Grid item sm={4} md={3} lg={2} style={{marginBottom: 25}}>
                                            <Grid container alignItems='center' justify='center'>
                                                <Grid item xs={12}>
                                                    <img
                                                        src={'https://image.tmdb.org/t/p/w240_and_h266_face/' + cast.profile_path} style={{width:'60%'}}/>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <Typography variant='body1'>
                                                        {cast.name} as
                                                    </Typography>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <Typography variant='body1'>
                                                        {cast.character}
                                                    </Typography>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    )
                                }

                            })}
                            </Grid>
                            {/*usar este link para boas profile pics https://image.tmdb.org/t/p/w240_and_h266_face/uutXEuNeovtiCH4yg06L5o5Oi1S.jpg*/}
                        </Typography>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel expanded={expanded === 'panel3'} onChange={this.handleChange('panel3')}>
                    <ExpansionPanelSummary ccexpandIcon={<ExpandMoreIcon />}>
                        <Typography>Collapsible Group Item #3</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Typography>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus
                            ex, sit amet blandit leo lobortis eget. Lorem ipsum dolor sit amet, consectetur
                            adipiscing elit. Suspendisse malesuada lacus ex, sit amet blandit leo lobortis eget.
                        </Typography>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            </div>
        );
    }
}

export default MovieExpansionAcc;