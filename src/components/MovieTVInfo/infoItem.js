import React, {Component} from 'react';
import NavBar from "../NavBar/NavBar";
import Grid from '@material-ui/core/Grid/Grid';
import '../../'
import Typography from "@material-ui/core/es/Typography/Typography";
import ScoreCircle from "./scoreCircle";
import TimeProgress from "./timeProgress";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import MovieExpansionAcc from "./movieExpansionAcc";
import $ from "jquery";
import GetSocial from "./getSocial";
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import MakeModal from "../../customStyling/makeModal";
import RangeValue from "../../customStyling/rangeValue";
import { connect } from 'react-redux'
import { rateMovie } from '../../store/actions/rateActions'
import SnackBarError from "../../customStyling/snackBarError";

let writeName;

class InfoItem extends Component {
    state={
        changeable:{
            info:'',
            setSocial: '',
            seasons:'',
            writeExpansion:'',
        },
        genres: [],
        writeIcons: '',
        rate:{
            idmovie:'',
            rating:'',
            seenValue:0,
            watchedValue:0,
        },
        getValues:{
            idmovie:'',
            rating:''
        },
        snackInfo:'',
        rating: '',
		getVideo:'',



    };
    componentDidMount() {
        this.getValues();
        let urlString;
        let pathId = this.props.match.params.path_id;
        if (this.props.match.params.movie === 'movie') {
            urlString = 'https://api.themoviedb.org/3/movie/' + pathId + '?api_key=f634db3b23ebcd55d865db0de4f007e7&append_to_response=videos'
        } else if (this.props.match.params.movie === 'tv') {
            urlString = 'https://api.themoviedb.org/3/tv/' + pathId + '?api_key=f634db3b23ebcd55d865db0de4f007e7&append_to_response=videos'
        }
        $.ajax({
            url: urlString,
            success: (searchResults) => {
                this.setState({
                    changeable: {
                        ...this.state.changeable,
                        info: searchResults,
                        setSocial: <GetSocial id={pathId} type={this.props.match.params.movie}/>,
                        writeExpansion:  <MovieExpansionAcc info={searchResults.seasons} id={pathId} type={this.props.match.params.movie}/>

                    },
                });
                this.setState({genres: searchResults.genres});
				if(searchResults.videos.results.length >0){
					this.setState({getVideo: <MakeModal content={
                                                                    <Grid item xs={6} style={{textAlign: 'center'}}>
                                                                        <Grid container alignItems='center' justify='center'>

                                                                            <FontAwesomeIcon icon={['fab', 'youtube']}
                                                                                             style={{
                                                                                                 fontSize: '3em',
                                                                                                 margin: 15, 
																								 color: 'white'
                                                                                             }}>
                                                                            </FontAwesomeIcon>
                                                                        </Grid>
                                                                    </Grid>} message={                        <Grid container alignItems='center' justify='center'>
                                                                    <iframe id="ytplayer" type="text/html" width="640" height="360"
                                                                                              src={'https://www.youtube.com/embed/' + searchResults.videos.results[0].key}
                                                                                              frameBorder="0"
                                                                                              allowFullScreen></iframe>
                                                                </Grid>
					}/>})
				}
				else{
					this.setState({getVideo: <Grid item xs={6} style={{textAlign: 'center'}}>
                                                                        <Grid container alignItems='center' justify='center'>

                                                                            <FontAwesomeIcon icon={['fab', 'youtube']}
                                                                                             style={{
                                                                                                 fontSize: '3em',
                                                                                                 margin: 15
                                                                                             }}>
                                                                            </FontAwesomeIcon>
                                                                        </Grid>
                                                                    </Grid>
																	
					})
				}
				

            },
            error: (xhr, status, err) => {
                console.log("fetch falhou");
            }
        });
        // setTimeout(() => {

        this._setIcons();
    }

    componentDidUpdate(prevProps, prevState) {
        const {auth, profile} = this.props;

        if (!auth.isEmpty) {
            if (prevState.rate.rating !== this.state.rate.rating || prevProps.getRate.rate.seen !== this.props.getRate.rate.seen || prevProps.getRate.rate.watched !== this.props.getRate.rate.watched) {
                if (prevProps.getRate.rate.seen !== this.props.getRate.rate.seen) {
                    if (this.props.getRate.rate.seen === 1) {
                        this.setState({snackInfo: <SnackBarError type={'success'} message={'You have seen it! :o'}/>});
                        this.setState({rate: {...this.state.rate, seenValue: this.props.getRate.rate.seen}})
                    }
                    setTimeout(() => {
                        this.setState({snackInfo: ''});
                    }, 2000);
                    this._setIcons();
                } else if (prevProps.getRate.rate.watched !== this.props.getRate.rate.watched) {
                    if (this.props.getRate.rate.watched === 1) {
                        this.setState({
                            snackInfo: <SnackBarError type={'warning'}
                                                      message={'You gonna watch it later! Do not forget that :|'}/>
                        });
                        this.setState({rate: {...this.state.rate, watchedValue: this.props.getRate.rate.watched}})

                    }
                    setTimeout(() => {
                        this.setState({snackInfo: ''});
                    }, 2000);
                    this._setIcons();
                } else {
                    this.setState({snackInfo: <SnackBarError type={'success'} message={'You liked this! :)'}/>});
                    setTimeout(() => {
                        this.setState({snackInfo: ''});
                    }, 2000);
                    this._setIcons();
                }
            }

            if (prevProps.auth !== this.props.auth) {
                if (prevProps.auth.isEmpty !== this.props.auth.isEmpty) {
                    this._setIcons();
                    /* Callback on SetState
                    this.setState({}, ()=>{

                    })*/
                }
            }
            if (this.props.getRate.rate.idRating !== undefined && this.props.getRate.rate.idRating !== prevProps.getRate.rate.idRating) {
                this.setState({
                    rate: {
                        ...this.state.rate,
                        rating: this.props.getRate.rate.idRating
                    }
                }, () => this._setIcons());
            }
        }
    }

    _setIcons = () =>{

        const {auth, profile} = this.props;

        if (!auth.isEmpty) {
            if (this.state.writeIcons === '' || this.state.rate.rating !== '' || this.props.getRate.rate.idRating !== undefined  || this.props.getRate.rate.seen !== undefined || this.props.getRate.rate.watched !== undefined) {
                let style;
                let styleEye= {color:'white'};
                let styleClock = {color:'white'};
                if(this.props.getRate.rate.seen !== undefined){
                    if(this.props.getRate.rate.seen === 1){
                        styleEye={
                            color:'blue'
                        }
                    }
                }

                if(this.props.getRate.rate.watched !== undefined){
                    if(this.props.getRate.rate.watched === 1){
                        styleClock={
                            color:'yellow'
                        }
                    }
                }

                if(this.props.getRate.rate.idRating > 0 && this.props.getRate.rate.idRating <= 100) {
                    style = {
                        color: 'red',
                    };
                }
                else if(this.state.rate.rating !== ''){
                    style = {
                        color: 'red',
                    };
                }

                else{
                    style = {
                        color: 'white',
                    };
                }

                this.setState({
                    writeIcons: <Grid item xs={12} className='bg-movie-tv' style={{maxWidth: '90%'}}>
                        <Grid container alignItems='center' justify='center'>
                            <Grid item xs={4}>
                                <MakeModal content={<Tooltip title='Like' style={style}>
                                    {/*color:'rgba(149, 43, 50, 0.9)'*/}
                                    <IconButton aria-label='Heart'>
                                        <FontAwesomeIcon icon='heart' style={{fontSize: '1.6em'}}>
                                        </FontAwesomeIcon>

                                    </IconButton>
                                </Tooltip>} message={<RangeValue value={this.state.rate.rating} toChange={this.onSubmitValue.bind(this)} name={writeName}
                                                                 type={this.props.match.params.movie}/>}/>
                            </Grid>
                            <Grid item xs={4}>
                                <Tooltip title='Seen' style={styleEye}>
                                    <IconButton aria-label='Seen' onClick={this.onSubmitViewed.bind(this)}>
                                        <FontAwesomeIcon icon='eye' style={{fontSize: '1.6em'}}/>
                                    </IconButton>
                                </Tooltip>
                            </Grid>
                            <Grid item xs={4}>
                                <Tooltip title='Watch Later' style={styleClock}>
                                    <IconButton aria-label='Clock' onClick={this.onSubmitLater.bind(this)}>
                                        <FontAwesomeIcon icon='clock' style={{fontSize: '1.6em'}}/>
                                    </IconButton>
                                </Tooltip>
                            </Grid>

                        </Grid>
                    </Grid>
                })
            }
            // }}, 100);
        }

    };

    getValues(){
        setTimeout(() => {
            this.setState({
                getValues: {
                    ...this.state.getValues,
                    idmovie: this.props.match.params.path_id,
                    rating: 101,
                    type: 'get',
                    todo: 'like'
                }
            }, () => this.props.rateMovie(this.state.getValues));
        },1000)

    }

    onSubmitValue(value) {
        this.setState({rate: {...this.state.rate, idmovie: this.props.match.params.path_id, rating: value, type:'update', todo:'like'}});
        setTimeout(() => {
            this.props.rateMovie(this.state.rate);
        }, 100);
    }

    onSubmitLater(){
        let value;
        if(this.state.rate.watchedValue === 0){
            value= 1;
        }
        else if(this.state.rate.watchedValue === 1){
            value = 0;
        }
        this.setState({rate: {...this.state.rate, idmovie: this.props.match.params.path_id, type:'update', todo:'watched', watchedValue: value}});
        setTimeout(() => {
            this.props.rateMovie(this.state.rate);
        }, 100);
    }

    onSubmitViewed(){
        let value;
        if(this.state.rate.seenValue === 0){
           value= 1;
        }
        else{
            value = 0;
        }
        this.setState({rate: {...this.state.rate, idmovie: this.props.match.params.path_id, type:'update', todo:'seen', seenValue: value}});
        setTimeout(() => {
            this.props.rateMovie(this.state.rate);
        }, 100);
}

    render() {
        //http://api.themoviedb.org/3/movie/157336?api_key=f634db3b23ebcd55d865db0de4f007e7&append_to_response=videos
       const info = this.state.changeable.info;
       let backdrop;
       let poster;
       let overview;
       let youtube;
if(this.state.changeable.info !== ''){
    overview = info.overview.substring(0, 610) + ' ...';



}
       if(info.backdrop_path !== 'null') {
           backdrop = "https://image.tmdb.org/t/p/original"+info.backdrop_path;
       }
       else {
           backdrop = "../../img/background_popsearch.jpg";
       }
        if(info.backdrop_path !== 'null') {
            poster = 'https://image.tmdb.org/t/p/original/' + info.poster_path;
        }
        else {
            poster = '../../img/poster_photo_movie.jpg';
        }
        let runtime;
        if(info.episode_run_time === undefined){
            runtime = info.runtime;
        }
        else{
            runtime = info.episode_run_time;
        }
        const percentPass = runtime/180*100;
       const getH = parseInt(runtime/60);
       const getM = runtime - (getH * 60);
       const percentScore = info.vote_average * 10;
       if(info.original_title === undefined){
           writeName = info.name;
       }
       else{
           writeName = info.original_title;

       }
       let taglineOrSeasons;
       if(info.tagline === undefined){
           taglineOrSeasons = <Typography variant="subtitle1" style={{color:'white'}}>{"Seasons: " + info.number_of_seasons + " with " + info.number_of_episodes +" Episodes"} </Typography>;
       }
       else{
           taglineOrSeasons = info.tagline;
       }
        return (
            <div>
                <NavBar/>
                <Grid container
                      spacing={20}
                      alignItems='center'
                      justify='center'
                      className='c-black text-center'
                      style={{marginTop: -64}}>

                    <div className='bg-no-blur'
                         style={{backgroundImage: 'url('+ backdrop+')'}}></div>
                    <div className='bg-opacity-black'></div>
                    <Grid item xs={12} style={{zIndex: 999, marginTop: 64, marginBottom: 64}}>
                        <Grid container
                              spacing={20}
                              alignItems='center'
                              justify='center'
                              style={{minHeight: '100vh'}}
                              className='c-black text-center'>
                            <Grid item xs={10}>
                                <Grid container className='c-white text-center' style={{marginTop: 0}}>
                                    <Grid item xs={4} style={{minHeight: '85%'}}>
                                        <img src={poster}
                                             style={{width: '90%', boxShadow: '10px 10px 5px 0px rgba(0,0,0,0.46)'}}/>
                                        <Grid container alignItems='center' justify='center'>
                                            {this.state.writeIcons}
                                        </Grid>
                                    </Grid>
                                    <Grid item xs={8} className='bg-movie-tv' style={{position:'static'}}>
                                        <Grid container alignItems='center' justify='center'>
                                            <Grid item xs={12} className='borderTeste'>
                                                <Grid item xs={12}>
                                                    <Typography variant='h4' style={{color:'white', fontWeight:'bold'}}>
                                                        {writeName}
                                                    </Typography>

                                                </Grid>
                                            </Grid>
                                            <Grid item xs={12} style={{maxHeight: '100%'}}>
                                                <Grid item xs={12}>
                                                    <Typography variant='subtitle1' style={{color:'white'}}>
                                                        {taglineOrSeasons}
                                                    </Typography>
                                                </Grid>
                                                <Grid item xs={12} style={{marginTop:25}}>
                                                    <Grid container alignItems='center' justify='center'>
                                                        <Grid item xs={2} style={{maxWidth:75}}>
                                                            <ScoreCircle percentage={percentScore}/>
                                                        </Grid>
                                                        <Grid item xs={4}>
                                                            <Typography variant='body1'>

                                                                {this.state.genres.map((genre) => {
                                                                    if (this.state.genres !== '') {
                                                                        return (
                                                                            <span style={{color:'white'}}>
                                                                                {genre.name + ' '}
                                                                            </span>
                                                                        )

                                                                    }
                                                                })
                                                                }
                                                            </Typography>

                                                        </Grid>
                                                        <Grid item xs={6}>
                                                            {this.state.changeable.setSocial}
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                                <Grid item xs={12} style={{marginTop: 25}}>
                                                    <Grid container alignItems='center' justify='center'>
                                                        <Grid item xs={10}>
                                                            <Grid container alignItems='center' justify='center'>

                                                                <Grid item xs={4}>
                                                                    <TimeProgress percent={percentPass} style={{minWidth:200}}/>
                                                                    {getH}h {getM}m
                                                                </Grid>
                                                                <Grid item xs={2}>
                                                                </Grid>
                                                                {this.state.getVideo}
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                                <Grid item xs={12} style={{marginTop: 50}}>
                                                    {overview}
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>

                    </Grid>
                </Grid>
                <Grid container
                      spacing={20}
                      alignItems='center'
                      justify='center'
                      className='c-black text-center'
                style={{marginTop: -64}}>
                              </Grid>
                {this.state.changeable.writeExpansion}
                {this.state.snackInfo}
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return{
        auth: state.firebase.auth,
        profile: state.firebase.profile,
        rate: state,
        getRate: state,

    }
};


const mapDispatchToProps = (dispatch) => {
    return {
        rateMovie: (value) => dispatch(rateMovie(value)),
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(InfoItem);