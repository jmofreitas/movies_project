import CircularProgressbar from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import '../../App.css'


import React, {Component} from 'react';

class ScoreCircle extends Component {
    render() {
        return (
            <div>
                <CircularProgressbar
                    percentage={this.props.percentage}
                    text={`${this.props.percentage}%`}

                    styles={{
                        path: { stroke: `rgba(62, 152, 199, ${this.props.percentage / 100})` },
                        text: { fill: '#f88', fontSize: '18px' },
                    }}
                />
            </div>
        );
    }
}

export default ScoreCircle;
