import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';


const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,

    },

    cssLabel: {
        color: 'white',
        '&$cssFocused': {
            color: 'yellow',
        },
        marginTop: -5
    },

    cssOutlinedInput: {
        color:'white',
        '&$cssFocused $notchedOutline': {
            borderColor: `yellow !important`,
            color: 'red'
        }
    },

    cssFocused: {},

    notchedOutline: {
        borderWidth: '1px',
        borderColor: '#FDD93C !important'
    },

});

class SearchBar extends React.Component {
    state={
        offset: 0,
        search: ''
    };

    onFieldChange(event) {
        // for a regular input field, read field name and value from the event
        const fieldName = event.target.name;
        const fieldValue = event.target.value;
        this.props.onChange(fieldName, fieldValue);
    }
    render() {
        const {classes} = this.props;


        return (
            <form className={classes.container} noValidate autoComplete="off">
            <TextField
                style={{color:'white'}}
                onChange={this.onFieldChange.bind(this)}
                className={classes.textField}
                margin="normal"
                variant="outlined"
                id="outlined-search"
                label="Search here"
                type="search"
                name="searcher"
                fullWidth
                InputLabelProps={{
                    classes: {
                        root: classes.cssLabel,
                        focused: classes.cssFocused,
                    },
                }}
                InputProps={{
                    classes: {
                        root: classes.cssOutlinedInput,
                        focused: classes.cssFocused,
                        notchedOutline: classes.notchedOutline,
                    },
                }}
            />
            </form>
        );
    }
}

SearchBar.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SearchBar);


