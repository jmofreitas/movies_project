import React, { Component } from 'react'
import Slider from 'react-rangeslider'
import Button from '@material-ui/core/Button';


class RangeValue extends Component {
    constructor (props, context) {
        super(props, context);
        this.state = {
            value: 10
        }
    }

    handleChangeStart = () => {
        console.log('Change event started')
    };

    handleChange = value => {
        this.setState({
            value: value
        })
    };

    handleChangeComplete = () => {
        console.log('Change event completed')
    };
    handleClick = () => {
        setTimeout(() => {
            this.props.toChange(this.state.value);
        }, 100)
    };
    componentDidMount() {
        this.setState({value: this.props.value});
    }

    render () {
        const { value } = this.state;
        console.log("PROPS " +this.props);

        return (
            <div className='slider' style={{textAlign:'center'}}>
                <p>Rank the {this.props.type}, {this.props.name} from 0 to 100!</p>
                <div className='value'>{value}</div>

                <Slider
                    min={0}
                    max={100}
                    value={value}
                    onChangeStart={this.handleChangeStart}
                    onChange={this.handleChange}
                    onChangeComplete={this.handleChangeComplete}
                />
                <Button variant="contained" onClick={this.handleClick}>
                    Submit
                </Button>
            </div>
        )
    }
}

export default RangeValue