import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

// Replace this with your own config details
let config = {
    apiKey: "AIzaSyBIhXtKwR5mSSVXWAfL4QfD3xzih76Ghpo",
    authDomain: "movies-project-lab5.firebaseapp.com",
    databaseURL: "https://movies-project-lab5.firebaseio.com",
    projectId: "movies-project-lab5",
    storageBucket: "movies-project-lab5.appspot.com",
    messagingSenderId: "441529805686"
};
firebase.initializeApp(config);
firebase.firestore().settings({ timestampsInSnapshots: true });

export default firebase;