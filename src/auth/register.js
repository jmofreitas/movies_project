import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import NavBar from "../components/NavBar/NavBar";
import SnackBarError from "../customStyling/snackBarError";
import {connect} from 'react-redux'
import {signUp} from '../store/actions/authActions'
import { Redirect } from 'react-router-dom'

const styles = theme => ({
    main: {
        width: 'auto',
        display: 'block', // Fix IE 11 issue.
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
            width: 400,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing.unit * 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    avatar: {
        margin: theme.spacing.unit,
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing.unit,
    },
    submit: {
        marginTop: theme.spacing.unit * 3,
    },
});

let email = '';
let username = '';
let password = '';
let passwordConfirm = '';
let ifError = '';


class Register extends Component {
    state={
        creds: {
            email: '',
            password: '',
            passwordConfirm: '',
            username: '',
        },
        ifError:'',
    };
    read(e){
        if(e.target.id === 'password'){
            this.setState({creds :{...this.state.creds, password: e.target.value}});

            console.log(password)
        }
        else if(e.target.id === 'passwordConfirm'){
            this.setState({creds :{...this.state.creds, passwordConfirm: e.target.value}});
            console.log(passwordConfirm)
        }
        else if(e.target.id === 'email'){
            this.setState({creds :{...this.state.creds, email: e.target.value}});

            console.log(email)
        }
        else if(e.target.id === 'username'){
            this.setState({creds :{...this.state.creds, username: e.target.value}});

            console.log(username)
        }
        this.setState({ifError: ''});
    }
    checkValues(){
        console.log('tette');
        console.log(password);
        if(this.state.creds.password !== '' && this.state.creds.passwordConfirm !== ''){
            console.log('1');
           if(this.state.creds.password !== this.state.creds.passwordConfirm || this.state.creds.password.length < 8){
                console.log('2');
                this.setState({ifError : <SnackBarError type={'error'}  message={'Verify your passwords! It must have 8 or more characters.'}/>})
            }
           else{
               this.handleSubmit();
               setTimeout( () => {
                   console.log('teste')
                   if(this.props.authError === 'The email address is already in use by another account.') {
                       this.setState({ifError : <SnackBarError type={'warning'} message={'This email adress is already in use!'}/>})

                   }} , 1000                );
           }
        }
    }

    handleSubmit = () => {
            console.log(this.state.creds);
        this.props.signUp(this.state.creds);
    }

    render() {
            console.log(this.state);
            console.log(this.props);

        const {classes} = this.props;
        const { auth } = this.props;
        if (auth.uid) return <Redirect to='/' />;

        return (
            <div>
                <NavBar/>
                {this.state.ifError}
                import { Redirect } from 'react-router-dom'

                <main className={classes.main}>
                    <CssBaseline/>
                    <Paper className={classes.paper}>
                        <Avatar className={classes.avatar}>
                            <img src='img/popsearch.png' style={{width: '65%'}}/>
                        </Avatar>
                        <Typography component="h1" variant="h5">
                            Register
                        </Typography>
                        <form className={classes.form} >
                            <FormControl margin="normal" required fullWidth>
                                <InputLabel htmlFor="email">Email Address</InputLabel>
                                <Input onChange={(e) => this.read(e)} id="email" name="email" autoComplete="email"
                                       autoFocus/>
                            </FormControl>
                            <FormControl margin="normal" style={{marginTop: -1}} required fullWidth>
                                <InputLabel htmlFor="text">Username</InputLabel>
                                <Input onChange={(e) => this.read(e)} id="username" name="text" autoComplete="text"/>
                            </FormControl>
                            <FormControl margin="normal" required fullWidth>
                                <InputLabel style={{marginTop: -15}} htmlFor="password">Password</InputLabel>
                                <Input onChange={(e) => this.read(e)} name="password" type="password" id="password"
                                       autoComplete="current-password"/>
                            </FormControl>
                            <FormControl margin='normal' required fullWidth>
                                <InputLabel style={{marginTop: -15}} htmlFor="passwordConfirm">Confirm
                                    Password</InputLabel>
                                <Input onChange={(e) => this.read(e)} name="passwordConfirm" type="password"
                                       id="passwordConfirm" autoComplete="current-password"/>
                            </FormControl>
                            <FormControlLabel
                                control={<Checkbox value="remember" color="primary"/>}
                                label="Remember me"
                            />
                            <Button
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                                onClick={() => this.checkValues()}
                            >
                                Sign in
                            </Button>
                        </form>
                    </Paper>
                </main>
            </div>
        );
    }
}
Register.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
    return {
        auth: state.firebase.auth,
        authError: state.auth.authError
    }
}

const mapDispatchToProps = (dispatch)=> {
    return {
        signUp: (creds) => dispatch(signUp(creds))
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(withStyles(styles) (Register));
