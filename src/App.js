import React, {Component} from 'react';
import './App.css';
import $ from 'jquery';
import {BrowserRouter, Route} from "react-router-dom";
import HomeIndex from "./components/HomePage/homeIndex";
import ItemIndex from "./components/ItemInfo/itemIndex";
import InfoItem from './components/MovieTVInfo/infoItem';
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab} from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFacebook, faHeart, faClock, faEye} from '@fortawesome/free-solid-svg-icons'
import SignIn from "./auth/signIn";
import Register from "./auth/register"


class App extends Component {
    render() {
        return (
            <BrowserRouter>
                <div>
                    <Route path="/" component={HomeIndex} exact/>
                    <Route path="/i" component={ItemIndex}/>
                    <Route path='/:movie/:path_id' component={InfoItem}/>
                    <Route path='/signin' component={SignIn}/>
                    <Route path='/register' component={Register}/>
                </div>
            </BrowserRouter>
        );
    }
}

library.add(fab, faHeart, faClock, faEye);

export default App;
